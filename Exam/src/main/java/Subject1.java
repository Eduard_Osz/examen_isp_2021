public class Subject1 {
    class S {
        public void metB() {

        }
    }

    class K extends S {

    }

    class L {
        private K k;
    }

    class J {

    }

    class I {
        private long t;
        private N n;

        public void f() {

        }

        public void i(J j) {

        }
    }

    class N {

    }
}

