

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JButton;

public class Subject2 {

    private JFrame frame;
    private JTextField textField;
    private JTextField textField2;
    private JTextField textField3;


    public Subject2() {
        frame = new JFrame();
        frame.setBounds(100, 100, 400, 150);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        textField = new JTextField();
        textField.setBounds(24, 11, 102, 20);
        frame.getContentPane().add(textField);

        textField2 = new JTextField();
        textField2.setBounds(148, 11, 102, 20);
        frame.getContentPane().add(textField2);
        textField3 = new JTextField();
        textField3.setEditable(false);
        textField3.setBounds(102, 57, 86, 20);
        frame.getContentPane().add(textField3);
        JButton btn = new JButton("Button");
        btn.setBounds(267, 11, 89, 20);
        btn.addActionListener(e -> {
            String text1 = textField.getText();
            String text2 = textField2.getText();
            textField3.setText(String.valueOf(text1.length() + text2.length()));
        });
        frame.getContentPane().add(btn);
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        Subject2 window = new Subject2();
    }


}
